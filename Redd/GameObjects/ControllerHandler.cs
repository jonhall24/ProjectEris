﻿using Microsoft.Xna.Framework.Input;

namespace GameObjects
{
    public class ControllerHandler
    {
        public IMovement GetMovement()
        {
            IMovement movement = new NoMovement();
            KeyboardState keyboardState = Keyboard.GetState();

            if (keyboardState.IsKeyDown(Keys.W))
                movement = new MoveUp();
            if (keyboardState.IsKeyDown(Keys.S))
                movement = new MoveDown();
            if (keyboardState.IsKeyDown(Keys.A))
                movement = new MoveLeft();
            if (keyboardState.IsKeyDown(Keys.D))
                movement = new MoveRight();

            return movement;
        }
    }
}