﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GameObjects
{
    public class GameManager
    {
        private Player _player;
        private StageManager _stageManager;
        private ContentManager _contentManager;
        private SpriteBatch _spriteBatch;
        private GraphicsDevice _graphicsDevice;

        public GameManager(Game game, SpriteBatch spriteBatch)
        {
            _contentManager = game.Content;
            _spriteBatch = spriteBatch;
            _graphicsDevice = game.GraphicsDevice;
            _stageManager = new StageManager(_spriteBatch, _graphicsDevice);

            Vector2 defaultPlayerPosition = new Vector2(50f, 50f);
            _player = new Player(defaultPlayerPosition, _spriteBatch, _contentManager);
        }

        public void Update(Game game, GameTime gameTime)
        {
            _player.Update(gameTime);
        }

        public void Draw()
        {
            _spriteBatch.Begin();

            _stageManager.Draw();
            _player.Draw();

            _spriteBatch.End();
        }
    }
}
