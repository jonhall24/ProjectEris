﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GameObjects
{
    public class Player
    {
        private Vector2 _position;
        private SpriteBatch _spriteBatch;
        private ContentManager _contentManager;
        private ControllerHandler _controllerHandler;

        public Texture2D _texture { get; private set; }

        public Player(Vector2 position, SpriteBatch spriteBatch, ContentManager contentManager)
        {
            _contentManager = contentManager;
            _spriteBatch = spriteBatch;
            _controllerHandler = new ControllerHandler();

            LoadContent();
            Reset(position);
        }

        private void LoadContent()
        {
            _texture = _contentManager.Load<Texture2D>("ThinkingYoshi");
        }

        private void Reset(Vector2 position)
        {
            _position = position;
        }

        public void Update(GameTime gameTime)
        {
            _position += _controllerHandler.GetMovement().GetDirection();
        }

        public void Draw()
        {
            _spriteBatch.Draw(_texture, _position, null, Color.White);
        }
    }
}
