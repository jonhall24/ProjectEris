﻿using Microsoft.Xna.Framework.Graphics;

namespace GameObjects
{
    public class PlayStage
    {
        private readonly TileMapGenerator _tileMap;

        public PlayStage(GraphicsDevice graphicsDevice)
        {
            _tileMap = new TileMapGenerator(graphicsDevice.Viewport.Bounds, graphicsDevice);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            _tileMap.Draw(spriteBatch);
        }
    }
}
