﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameObjects
{
    public class TileMapGenerator
    {
        private int _tileWidth = 32;
        private int _tileHeight = 32;
        private GraphicsDevice _graphicsDevice;
        private Rectangle _viewport;
        private Dictionary<Texture2D, Vector2> _tileMap;

        public TileMapGenerator(Rectangle viewport, GraphicsDevice graphicsDevice)
        {
            _viewport = viewport;
            _graphicsDevice = graphicsDevice;
            _tileMap = GenerateTileMap();
        }

        private Dictionary<Texture2D, Vector2> GenerateTileMap()
        {
            Dictionary<Texture2D, Vector2> tileMap = new Dictionary<Texture2D, Vector2>();

            float tilePositionX = _viewport.X;
            float tilePositionY = _viewport.Y;
            int rows = _viewport.Width / _tileHeight;
            int columns = _viewport.Height / _tileWidth * 2;

            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    tileMap.Add(GetTileData(), new Vector2(tilePositionX, tilePositionY));
                    tilePositionX += _tileWidth;
                }
                tilePositionY += _tileHeight;
                tilePositionX = _viewport.X;
            }

            return tileMap;
        }

        private Texture2D GetTileData()
        {
            Texture2D tile = new Texture2D(_graphicsDevice, _tileWidth, _tileHeight);

            Color[] data = new Color[_tileWidth * _tileHeight];
            for (int i = 1; i < data.Length; i++)
                data[i] = Color.Black;
            tile.SetData(data);

            return tile;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var tile in _tileMap)
                spriteBatch.Draw(tile.Key, tile.Value, Color.White);
        }
    }
}
