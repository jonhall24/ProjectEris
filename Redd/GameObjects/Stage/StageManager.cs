﻿using Microsoft.Xna.Framework.Graphics;

namespace GameObjects
{
    public class StageManager
    {
        private SpriteBatch _spriteBatch;
        private PlayStage _playStage;

        public StageManager(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice)
        {
            _spriteBatch = spriteBatch;
            _playStage = new PlayStage(graphicsDevice);
        }

        public void Draw()
        {
            _playStage.Draw(_spriteBatch);
        }
    }
}
