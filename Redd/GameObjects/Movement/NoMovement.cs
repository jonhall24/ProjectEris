﻿using Microsoft.Xna.Framework;

namespace GameObjects
{
    public class NoMovement : IMovement
    {
        private Vector2 direction = new Vector2(0f, 0f);

        public Vector2 GetDirection()
        {
            return direction;
        }
    }
}