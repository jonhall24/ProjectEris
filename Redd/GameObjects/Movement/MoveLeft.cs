﻿using Microsoft.Xna.Framework;

namespace GameObjects
{
    public class MoveLeft : IMovement
    {
        private Vector2 direction = new Vector2(-1f, 0f);

        public Vector2 GetDirection()
        {
            return direction;
        }
    }
}