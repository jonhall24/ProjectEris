﻿using Microsoft.Xna.Framework;

namespace GameObjects
{
    public class MoveDown : IMovement
    {
        private Vector2 direction = new Vector2(0f, 1f);

        public Vector2 GetDirection()
        {
            return direction;
        }
    }
}