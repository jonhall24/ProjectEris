﻿using Microsoft.Xna.Framework;

namespace GameObjects
{
    public interface IMovement
    {
        Vector2 GetDirection();
    }
}